package com.ankir.testcwithoutsupport;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.ankir.testcwithoutsupport.wpiAndroid.wiringPiI2CRead;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("wiringPi");
    }

    static public native int wiringPiI2CRead(int fd);

Button mButton;
    TextView text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
mButton = (Button) findViewById(R.id.b);
text = (TextView)findViewById(R.id.text_);

mButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int i = wiringPiI2CRead (0);
        text.setText(i+"");
    }
});



    }
}
