package com.ankir.testcwithoutsupport;

/**
 * Created by UBCWindows on 19.12.2017.
 */

public final class wpiAndroid {
    static {
        System.loadLibrary("wiringPi");
    }

    static public native int wiringPiI2CRead(int fd);

}
